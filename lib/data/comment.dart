class Comment {
  // ignore: constant_identifier_names
  static const TAG = 'Comment';

  String? avatar;
  String? userName;
  String? content;
  int? id=1;
  String? date;
  List<dynamic>? tags = [];

  Comment({
    required this.avatar,
    required this.userName,
    required this.content,
    this.date,
    this.id,
    this.tags
  });
}
